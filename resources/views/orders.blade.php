<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <title>Orders</title>
</head>
<body class='bg-blue-300'>
    <main class='container md:m-auto'>
        <nav class='flex flex-row justify-between m-10'>
        <a href="{{ url('/') }}" class='font-bold text-3xl'>Empresa ABC</a>
            <p> Miércoles 10 de Febrero,12:25 pm</p>
        </nav>
        <div class="flex flex-row">
            <div class='w-2/6 bg-white rounded m-5'>
                <p class='m-5'>Número de ORDEN</p>
                <div class='p-1'>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                    <div class='flex flex-row justify-between items-center rounded border-gray-900 mb-2 hover:bg-blue-700 hover:text-white cursor-pointer'>
                        <p class='text-center py-2 px-4'>#{{ mt_rand(100000, 999999) }}</p>
                        <a href="{{ url('/orders/selected') }}" class='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>Preparado</a>
                    </div>
                </div>
            </div>
            <div class="w-auto flex flex-col m-5">
                <div class="container absolute w-1/2 right-auto">
                    <div class="mb-4">
                        <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight   text-center font-bold'>Estatus de ordenes general por dia</p>
                    </div>
                    <form class='rounded grid grid-rows-3 grid-flow-col gap-4'>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Ordenes Recividas: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Preparados: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Listas: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>En Camino: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Entregadas: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Canceladas: 15</p>
                        </div>
                        <div class="mb-4">
                            <p class='shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight  '>Total de Ordenes: 15</p>
                        </div>
                    </form>
                    <div class='flex container absolute w-full h-full right-auto bg-red-200'>
                        <img class='w-full' src="https://oobrien.com/wordpress/wp-content/uploads/2016/07/googlemaps_july2016.jpg" alt="google map">
                        <div class='w-28 h-28 rounded-full bg-red-600 z-10 absolute top-5 left-80 opacity-75'></div>
                        <div class='w-28 h-28 rounded-full bg-blue-600 z-10 absolute top-20 left-16 opacity-75'></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>