# Prueba de Concepto

## Instalacción

Deben de usar el paquete de control de versiones [git](https://git-scm.com/downloads), [Laraver](https://laravel.com/docs/8.x/installation), [Tailwindcss](tailwindcss.com) para la parte visual y [VSCode](https://code.visualstudio.com/) para así poder editar el proyecto.

```bash
git clone https://gitlab.com/Victor1890/proyect-test-php-laravel.git
```

```bash
cd proyect-test-php-laravel
```

En los archivos que aparecen en la carpera `resources/views` deben editar los archivos `orders.blade.php` y `select.blade.php` para así poder editar las vistas.

```bash
cd resources/views
```


## Routes ó Rutas

Usando VSCode, open the files `web.php` para agregar o editar las rutas,

```php
Route::get('/', function () {
    return view('welcome');
});

Route::get('orders', function(){
    return view('orders');
});

Route::get('orders/selected', function(){
    return view('select');
});
```

## Screenshot

![](design/img1.png)
![](design/img2.png)
![](design/img3.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)
